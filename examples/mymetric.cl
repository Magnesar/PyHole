/* Majumdar-Papapetrou metric in Cartesian coordinates. */

// No user arguments to be passed down from the kernel
#define USER_ARGS_DEF
#define USER_ARGS


// Compute the components of the metric at x and store them in the metric g.
bool updateMetric( const realV x, metric* restrict g USER_ARGS_DEF )
{
    const real X = x.s1;
    const real Y = x.s2;
    const real Z = x.s3;
    const real r1 = sqrt(X*X + Y*Y + (Z-CONST_A)*(Z-CONST_A));
    const real r2 = sqrt(X*X + Y*Y + (Z+CONST_A)*(Z+CONST_A));
    const real f1 = 1.0/r1;
    const real f2 = 1.0/r2;
    const real U = 1.0 + CONST_M1*f1 + CONST_M2*f2;
    const real U2 = U*U;

    // contravariant form
    g->tt = -U2;
    g->tx = 0.0;
    g->ty = 0.0;
    g->xx = 1.0/U2;
    g->yy = g->xx;
    g->zz = g->xx;
    g->xy = 0.0;
    g->xz = 0.0;
    g->yz = 0.0;

    g->dx_tt = 2.0*U*X*(CONST_M1*f1*f1*f1+CONST_M2*f2*f2*f2);
    g->dx_tx = 0.0;
    g->dx_ty = 0.0;
    g->dx_xx = 2.0/(U*U2)*X*(CONST_M1*f1*f1*f1+CONST_M2*f2*f2*f2);
    g->dx_yy = g->dx_xx;
    g->dx_zz = g->dx_xx;
    g->dx_xy = 0.0;
    g->dx_xz = 0.0;
    g->dx_yz = 0.0;

    g->dy_tt = 2.0*U*Y*(CONST_M1*f1*f1*f1+CONST_M2*f2*f2*f2);
    g->dy_tx = 0.0;
    g->dy_ty = 0.0;
    g->dy_xx = 2.0/(U*U2)*Y*(CONST_M1*f1*f1*f1+CONST_M2*f2*f2*f2);
    g->dy_yy = g->dy_xx;
    g->dy_zz = g->dy_xx;
    g->dy_xy = 0.0;
    g->dy_xz = 0.0;
    g->dy_yz = 0.0;

    g->dz_tt = 2.0*U*(CONST_M1*(Z-CONST_A)*f1*f1*f1+CONST_M2*(Z+CONST_A)*f2*f2*f2);
    g->dz_tx = 0.0;
    g->dz_ty = 0.0;
    g->dz_xx = 2.0/(U*U2)*(CONST_M1*(Z-CONST_A)*f1*f1*f1+CONST_M2*(Z+CONST_A)*f2*f2*f2);
    g->dz_yy = g->dz_xx;
    g->dz_zz = g->dz_xx;
    g->dz_xy = 0.0;
    g->dz_xz = 0.0;
    g->dz_yz = 0.0;

    return (r1<CONST_EH1) || (r2<CONST_EH2);
}
