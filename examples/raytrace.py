# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple example to compute a small image from scratch.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module if not installed system-wide
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image

# 1) Set up one of the metrics provided with PyHole
g = metric.Flat()
#g = metric.CFlat()
#g = metric.Schwarzschild(2.0)
#g = metric.Kerr(2.0, 1.0)
#g = metric.HR(metric.hr.Flat())
#g = metric.HR(metric.hr.Schwarzschild(2.0))
#g = metric.HR(metric.hr.Interpolated('hr-III.npz'))
#g = metric.CHR(metric.hr.Flat())
#g = metric.CHR(metric.hr.Schwarzschild(2.0))
#g = metric.CHR(metric.hr.Interpolated('hr-III.npz'))

# 2) set up an observer
o = observer.Equirectangular(r=15.0, theta=np.pi/2)
#o = observer.Equirectangular(r=15.0, theta=0.0)
#o = observer.Gnomonic(r=15.0, theta=np.pi/2)
#o = observer.Stereographic(r=15.0, theta=np.pi/2)

# 3) set up a propagator (the coordinates must match those of the metric, else an error is thrown)
p = propagator.SphericalCPU(o, g, Rsky=30.0)                        # Convenient Python SciPy based propagator for spherical coordinates
#p = propagator.SphericalGPU(o, g, Rsky=30.0, device="CPU")         # OpenCL based propagator optimized for speed but requires some (Py)OpenCL setup.
#p = propagator.CartesianCPU(o, g, Rsky=30.0)
#p = propagator.CartesianGPU(o, g, Rsky=30.0, device="CPU")
#p.real = np.float32    # some OpenCL implementations do not support double precision floating point (the default)
p.TOLERANCE = 1e-6      # the propagator error tolerance. Around 1e-6 is fast, around 1e-10 is accurate.
#p.PLATFORM_ID = 1      # sometimes it's necessary to specify the OpenCL platform to use (details on installed platforms: run "clinfo" on command line)

# generate and save an image
i = Image(p, (128,128))         # (128,128) is the image size in pixels.
i.saveImage('example.png')
print(str(i))                   # prints various statistics about the image
