# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple example of a user defined metric.

@author: Alexander Wittig
"""

# only needed to set up the relative path to local pyhole module if not installed system-wide
#import sys, os
#sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
import matplotlib.pyplot as pyplot
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image
from gr_pyhole.display import Display

# import the already set up metric object g, and propagator p defined in the mymetric file so we don't have to repeat the code here:
from mymetric import g, p

# 1) load the previously generated image from a file
i = Image()                    # empty image object
i.load('mymetric.npz')         # load the previously computed results

# 2) Print various information about the black hole image
print(str(i))

# 3) since the standard image is not what we want in this case, produce a custom colored image ourselves
def f(theta, phi, x):
    """Returns 4 color values (red, green, blue, alpha) where (0,0,0,0) is
    fully transparent black and (1.0, 1.0, 1.0, 1.0) is fully opaque white.

    This function is called with three arguments: theta, phi, x.
    x contains the final 4 position coordinates and 4 associated momenta (depending on the coordinate system of the metric)
    theta and phi are a pair of angles indicating various things:
            - when theta is positive, these are the coordinates on the celestial sphere where the trajectory intersects
            - when theta == -100.0, phi indicates various integrator errors (unknown error 1, out of time 2, too much null condition violation 3).
            - otherwise the ray fell into one of the black holes and the spherical coordinates of the point on the event horizon are given by theta+2*pi and phi+2*pi.
    """
    if theta < 0.0 and phi < 0.0:           # this one fell in
        if abs(x[3]-g.a)<0.11:              # x contains [t, x, y, z, ...] => z = x[3]
            return (1.0, 0.5, 0.5, 1.0)     # z coordinate near +a
        elif abs(x[3]+g.a)<0.11:
            return (0.5, 1.0, 0.5, 1.0)     # z coordinate near -a
        else:
            return (0.0, 0.0, 0.0, 1.0)     # weird, didn't fall in either one. Shouldn't exist, so mark black to check.
    else:
        return (1.0, 1.0, 1.0, 1.0)         # all others escaped: white

image = i.mapToImage(f)     # apply the above function to each pixel of the data to generate a new image
pyplot.imshow(image)        # show the new image in a separate window (non-interactive)
pyplot.imsave('mymetric-analysis.png', image)   # save the new image to a file

# 4) perform an interactive analysis of the image
i.updateBackground(bgfile='data/backgrounds/bg-color.png', lines=18)        # set a new background image on the celestial sphere with 18 lines per 180 degrees and the given background image
d = Display(i, p)       # create new Display for the image with associated propagator p (note that p currently must be a CPU propagator)
d.show()                # show the interactive main window
d.showCustomImage(image)    # show our previously generated image as the background (interactive)
