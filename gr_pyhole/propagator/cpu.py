# -*- coding: utf-8 -*-

#   Copyright 2015 - 2017 Alexander Wittig, Jai Grover
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# What is to be imported by "from cpu import *"
__all__ = ["CPU", "SphericalCPU", "CartesianCPU"]

from math import sqrt, sin, cos, acos, atan2
from scipy.integrate import ode
import numpy as np
from .propagator import Propagator

class CPU(Propagator):
    """A CPU propagator using SciPy integrators.
    This is an abstract class that is subclassed by propagators for specific coordinates."""

    def __init__(self, o, g, Rsky=0.0, integrator='vode'):
        """Create a new Propagator.

        :param o: The observer associated with this propagator.
        :param g: The metric associated with this propagator.
        :param integrator: The integrator to use.
        :param Rsky: The celestial sphere radius or 0.0 for infinite radius
        """
        super(CPU,self).__init__(o, g, Rsky)
        self.integrator = integrator
        self.error = False
        self.finishing = False

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Integrator: {}\n'.format(self.integrator)
        res += super(CPU,self).__str__()
        return res

    def generateImageData(self, size=(100,100), NP=1, I=0):
        """Generate directions table and set it in the camera / return it.
        If called with NP=1 it performs the whole computation in a single process
        and stores them directly in the camera.
        If called with NP>1, only partial results are computed and returned,
        while nothing is stored in the camera.


        :param size: The size of the directions table (= resolution of the resulting image).
        :param NP: The total number of slices (or processors) for parallel computation.
        :param I: The number of the slice of the directions table (for the i-th processor) to return.
        """
        if I>=NP: raise ValueError('Process index exceeds number of processes!')
        X = np.linspace(self.o.zoom[0][0],self.o.zoom[0][1],size[0])
        Y = np.linspace(self.o.zoom[1][1],self.o.zoom[1][0],size[1])       # directions[0,0] corresponds to top-left corner of image, so flip y limits
#        Y = np.linspace(self.o.zoom[1][0],self.o.zoom[1][1],size[1])       # directions[0,0] corresponds to bottom-left corner of image
        Y = Y[I::NP]            # cut out subsection of Y
        # perform the first integration here to know how much data we need to store
        temp = self.propagate(self.o.unproject(X[0],Y[0]))
        res = list(temp[0])+list(temp[1][-1])
        directions = np.empty((len(Y), len(X), len(res)))
        directions[0,0,:] = res
        for i, y in enumerate(Y):
            if self.VERBOSE:
                print("{}{}{}% done".format('' if NP==1 else I, '' if NP==1 else ': ', 100*i/len(Y)))
            for j, x in enumerate(X):
                if i==0 and j==0: continue      # we already did the first one
                temp = self.propagate(self.o.unproject(x,y))
                directions[i,j,:] = list(temp[0])+list(temp[1][-1])
        return directions

    def combinePartialImageData(self, size, NP, res):
        """Merge partial directions table and return complete data set.

        :param size: The size of the directions table.
        :param NP: The total number of slices (or processors) for parallel computation.
        :param res: List of the results of all slices generated by \see generateDirections
                    res = [i in range(NP): generateDirections(size, NP, i)]
        """

        data = np.empty((size[1], size[0], res[0].shape[2]))
        for i in range(len(res)):
            data[i::NP,:,:] = res[i]
        return data

    def propagate(self, ic, store=False, dt=10.0):
        """Propagation of a ray and final angle calculation. This performs a
        backward propagation in time, but the initial condition is given in
        forward time (i.e. the velocity is the actual forward-time velocity).

        :param ic: Initial condition as position and viewing angle ``[t,r,theta,phi,alpha,beta]`` (see :meth:`getMomenta`) or directly as ``[t,r,theta,phi,pt,pr,ptheta,pphi]``
        :param store: If true, return the trajectory every integration interval
        :param dt: Absolute length of one integration interval (within integration is performed with automatic step size control)
        :param tmax: Absolute maximum integration time
        :return: ((theta,phi), trajectory).
            Resulting angles are normalized to [0,2pi) for phi and [0,pi) for theta.

            - In case of collissions with the black hole, theta-2pi, phi-2pi is returned
            - In case of errors (-100.0,3.0) is returned.
            - In case of trapped rays (-100.0,2.0) is returned.

            Trajectory contains the points along the trajectory (in rows), in all cases there is at least the initial and final point.
            The trajectory data is always: integration variable lambda, the full state (4 position + 4 momenta), the winding number, the total winding number, the null condition violation (if available, else -1.0), and any other integrator dependent data
        """
        self.error = False
        self.finishing = False
        deltat = -dt        # integrate backwards in time!
        step = self.getMomenta(ic)
        rk = ode(self).set_integrator(self.integrator, atol=self.TOLERANCE, rtol=self.TOLERANCE, max_step=1.0, nsteps=max(500, int(5000*dt)))
        #rk.set_solout(self.checkState)      # not all NumPy integrators support this. Quite poorly done in np.
        rk.set_initial_value(step, 0.0)
        if store:
            res = np.empty((int(self.LMAX/dt)+2, len(step)+1))
            rp = 1
        else:
            res = np.empty((2, len(step)+1))
        res[0,0] = 0.0
        res[0,1:] = step
        while deltat != 0.0 and (not self.error) and rk.t > -self.LMAX and rk.successful():
            step = rk.integrate(rk.t+deltat)
            #step = rk.integrate(rk.t-tmax)         # use this together with checkState callback in set_solout
            if store:
                res[rp,0] = rk.t
                res[rp,1:] = step
                rp += 1
            deltat_new = self.getNextStep(rk.t, step, deltat)
            # WARNING: vode is not able to reverse integration direction
            # Hack around it by reinitializing if direction changes (shouldn't be too often)
            if deltat*deltat_new<0.0 and self.integrator=='vode':
                rk.set_initial_value(step, rk.t)
            deltat = deltat_new
        if store:
            res = res[0:rp, :]
        else:
            res[1,0] = rk.t
            res[1,1:] = step
        if self.error:
            # fell in
            theta, phi = self.getFinalDirection(rk.t, step, False)
            return ((theta-2.0*np.pi, phi-2.0*np.pi), res)
        elif not rk.successful():
            return ((-100.0, 3.0), res)         # in case of integrator error (e.g. out of steps) return (-1,-3)
        elif deltat!=0.0:
            return ((-100.0, 2.0), res)         # in case of out of integration time return (-1, -2)
        else:
            # compute final velocity direction and normalize
            theta, phi = self.getFinalDirection(rk.t, step, True)
            return ((theta,phi), res)

    def checkState(self, l, x):
        """Callback for ODE integration to terminate integration in case of
        error or when done.

        :param l: Current integration variable lambda.
        :param x: Current state.
        """
        if self.error:
            return -1
        else:
            return 0

    def getMomenta(self, x):
        """Convert initial point given as either x = (t,r,theta,phi,alpha,beta)
        or x = (t,r,theta,phi,pt,pr,ptheta,pphi) into an initial condition
        suitable for the propagator.

        :param x: Initial position and viewing angles (t,r,theta,phi,alpha,beta)
            or complete state (t,r,theta,phi,pt,pr,ptheta,pphi).
            Position is always specified in spherical (BL) coordinates.
        :returns: a vector with 4 state variables and 4 momenta
            as well as one additional variable for the total phi variation.
        """
        raise NameError('Must be implemented by subclass!')

    def getNextStep(self, l, x, dl):
        """Get the next integration time step for the current integration.

        :param l: current integration time lambda
        :param x: current integration state
        :param dl: the last integration time step.
        """
        raise NameError('Must be implemented by subclass!')

    def getFinalDirection(self, l, x, escaped):
        """Get the final direction where the light ray is coming from.

        :param l: Independent integration variable lambda.
        :param x: final integration state.
        :param escaped: flag to indicate if this is the final direction of a ray that escaped (True) or one that fell into a black hole (False)
        """
        raise NameError('Must be implemented by subclass!')

    def __call__(self, l, x):
        """Right hand side of the ODE for propagation.

        :param l: Independent integration variable lambda.
        :param x: Point where to evaluate the RHS.
        """
        raise NameError('Must be implemented by subclass!')


class SphericalCPU(CPU):
    """Propagator in spherical (BL) coordinates. Singular along the z axis but
    the natural choice of coordinates otherwise."""
    def __init__(self, o, g, integrator='vode', Rsky=0.0):
        """Create a new Propagator.

        :param o: The observer associated with this propagator.
        :param g: The metric associated with this propagator.
        :param integrator: The integrator to use.
        :param Rsky: The celestial sphere radius or 0.0 for infinite radius
        """
        if g.COORDINATES != "spherical":
            raise ValueError('This propagator requires a metric expressed in spherical coordinates (not {})!'.format(g.COORDINATES))
        super(SphericalCPU,self).__init__(o, g, Rsky, integrator);

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Propagator: Spherical CPU\n'
        res = res + super(SphericalCPU,self).__str__();
        return res

    def getMomenta(self, x):
        """Convert initial point given as either x = (t,r,theta,phi,alpha,beta)
        or x = (t,r,theta,phi,pt,pr,ptheta,pphi) into an initial condition
        suitable for the propagator.

        :param x: Initial position and viewing angles (t,r,theta,phi,alpha,beta)
            or complete state (t,r,theta,phi,pt,pr,ptheta,pphi).
            Position is always specified in spherical coordinates.
        :returns: a vector with 4 state variables and 4 momenta
            as well as three additional variables for the phi variation, total phi
            variation, and the null condition violation (not computed).
        """
        if len(x)==6:
            # viewing angles to spherical momenta
            sa = np.sin(x[4])
            ca = np.cos(x[4])
            sb = np.sin(x[5])
            cb = np.cos(x[5])
            self.g.setPoint(x[0:4])
            [g_tt, g_rr, g_thth, g_pp, g_tp] = self.g.toLower()
            det = g_tt*g_pp-g_tp*g_tp
            res = [x[0], x[1], x[2], x[3], 0.0, 0.0, 0.0, 0.0]
            res[4] = -np.sqrt(-det/g_pp) - sa*cb*g_tp/np.sqrt(g_pp)
            res[5] = ca*np.sqrt(g_rr)
            res[6] = -sa*sb*np.sqrt(g_thth)
            res[7] = -sa*cb*np.sqrt(g_pp)
            return res+[0.0, 0.0, -1.0]
        elif len(x)==8:
            # full state
            return list(x)+[0.0, 0.0, -1.0]
        else:
            # unknown
            return x

    def getNextStep(self, l, x, dl):
        """Get the next integration time step for the current integration.

        :param l: current integration time lambda
        :param x: current integration state
        :param dl: the last integration time step.
        """
        y = self(l, x)
        if self.Rsky == 0.0:
            # integrating backward => outward leg has negative radial change
            #done = x[1]*y[1]<0 and abs(x[1])>50*self.g.EH   # on an outward leg and far away
            #done = x[1]*y[1]<0 and abs(y[2])<1e-3 and abs(y[3])<1e-3   #  on an outward leg and small angular change
            done = x[1]*y[1]<0.0 and abs(y[2])<1e-3 and abs(y[3])<1e-3   # on an outward leg and either one of the above
            if done:
                return 0.0
            else:
                return dl
        else:
            # cutoff at given fixed radius
            # are we there?
            if abs(x[1]-self.Rsky)<1e-2:
                return 0.0
            # are we getting close?
            if not self.finishing:
                xdotv = x[1]*y[1]
                self.finishing = xdotv<0.0 and x[1]>0.95*self.Rsky    # on outward leg and close enough to Rsky
            # if we're in the finishing stage, do a Newton to get to Rsky
            if self.finishing:
                return 0.95*(self.Rsky-x[1])/y[1]    # damping factor to avoid overshooting
            else:
                return dl

    def getFinalDirection(self, l, step, escaped):
        """Get the final direction where the light ray is coming from.

        :param l: Independent integration variable lambda.
        :param step: final integration state.
        :param escaped: flag to indicate if this is the final direction of a ray that escaped (True) or one that fell into a black hole (False)
        """
        if self.Rsky == 0.0 and escaped:
            df = self(l, step)
            vx = -(df[1]*sin(step[2])*cos(step[3])+df[2]*step[1]*cos(step[2])*cos(step[3])-df[3]*step[1]*sin(step[2])*sin(step[3]))
            vy = -(df[1]*sin(step[2])*sin(step[3])+df[2]*step[1]*cos(step[2])*sin(step[3])+df[3]*step[1]*sin(step[2])*cos(step[3]))
            vz = -(df[1]*cos(step[2])-df[2]*step[1]*sin(step[2]))
            theta = acos(vz/sqrt(vx*vx+vy*vy+vz*vz))
            phi = atan2(vy, vx)
        else:
            # just return the current angles
            theta = step[2]
            phi = step[3]
        # adjust for border cases when r became negative
        if step[1]<0.0:
            theta = np.pi - theta
            phi = phi + np.pi
        # normalize
        theta = theta%(2.0*np.pi)
        phi = phi%(2.0*np.pi)
        if theta>np.pi:
            theta = 2.0*np.pi - theta
            phi = (phi+np.pi)%(2.0*np.pi)
        return (theta,phi)

    def __call__(self, l, x):
        """Right hand side of the ODE for propagation.

        :param l: Independent integration variable lambda.
        :param x: Point where to evaluate the RHS.
        """
        y = np.zeros(11)
        if self.error or self.g.setPoint(x):
            self.error = True
            return y
        y[0] = x[4]*self.g.tt+x[7]*self.g.tp
        y[1] = x[5]*self.g.rr
        y[2] = x[6]*self.g.thth
        y[3] = x[7]*self.g.pp+x[4]*self.g.tp
#        y[4] = 0.0         # already initialized to zero
        y[5] = -0.5*(x[4]*x[4]*self.g.dr_tt +x[5]*x[5]*self.g.dr_rr +x[6]*x[6]*self.g.dr_thth +x[7]*x[7]*self.g.dr_pp) -x[4]*x[7]*self.g.dr_tp
        y[6] = -0.5*(x[4]*x[4]*self.g.dth_tt+x[5]*x[5]*self.g.dth_rr+x[6]*x[6]*self.g.dth_thth+x[7]*x[7]*self.g.dth_pp)-x[4]*x[7]*self.g.dth_tp
#        y[7] = 0.0
        y[8] = y[3]         # to compute variation in phi (same as phi for spherical)
        y[9] = abs(y[3])    # to compute total variation in phi
#        y[10] = 0.0        # null condition violation, not computed on CPU
        return y

class CartesianCPU(CPU):
    """Propagator in Cartesian coordinates. Requires a compatible Cartesian-capable metric!"""
    def __init__(self, o, g, integrator='vode', Rsky=0.0):
        """Create a new Propagator.

        :param o: The observer associated with this propagator.
        :param g: The metric associated with this propagator.
        :param integrator: The integrator to use.
        :param Rsky: The celestial sphere radius or 0.0 for infinite radius
        """
        if g.COORDINATES != "Cartesian":
            raise ValueError('This propagator requires a metric expressed in Cartesian coordinates (not {})!'.format(g.COORDINATES))
        super(CartesianCPU,self).__init__(o, g, Rsky, integrator)

    def __str__(self):
        """Show human readable summary of the current setup.
        """
        res = 'Propagator: Cartesian CPU\n'
        res = res + super(CartesianCPU,self).__str__();
        return res

    def getMomenta(self, x):
        """Convert initial point x = (t,r,theta,phi,alpha,beta)
        into an initial condition suitable for the propagator.

        :param x: Initial position and viewing angles (t,r,theta,phi,alpha,beta).
            Position is always specified in spherical coordinates.
        :returns: a vector with 4 state variables [t,x,y,z] and 4 momenta
            [pt,px,py,pz] as well as three additional variables for phi and the
            total variation in phi, and null condition violation (not computed on CPU).
        """
        if len(x)==6:
            # viewing angles to momenta
            st = np.sin(x[2])
            ct = np.cos(x[2])
            sp = np.sin(x[3])
            cp = np.cos(x[3])
            sa = np.sin(x[4])
            ca = np.cos(x[4])
            sb = np.sin(x[5])
            cb = np.cos(x[5])
            r = x[1]
            res = [x[0], st*cp*r, st*sp*r, ct*r, 0.0, 0.0, 0.0, 0.0]
            # get contravariant spherical (hat) from contravariant cartesian
            self.g.setPoint(x[0:4])
            gtt = self.g.tt
            gtp = -sp*self.g.tx + cp*self.g.ty        # wiggle
            grr = st*st*cp*cp*self.g.xx + st*st*sp*sp*self.g.yy + ct*ct*self.g.zz + 2.0*st*st*sp*cp*self.g.xy + 2.0*st*ct*cp*self.g.xz + 2.0*st*ct*sp*self.g.yz
            gthth = (ct*ct*cp*cp*self.g.xx + ct*ct*sp*sp*self.g.yy + st*st*self.g.zz + 2.0*ct*ct*sp*cp*self.g.xy - 2.0*st*ct*cp*self.g.xz - 2.0*st*ct*sp*self.g.yz)/(r*r)
            gpp = sp*sp*self.g.xx+cp*cp*self.g.yy-2.0*sp*cp*self.g.xy       # wiggle
            det = 1.0/(gtt*gpp-gtp*gtp)     # wiggle, covariant determinant
            # compute momenta
            res[4] = -1.0/np.sqrt(-gtt) + sa*cb*gtp/np.sqrt(det/gtt)
            res[5] = st*cp*ca/np.sqrt(grr) + sp*sa*cb*np.sqrt(gtt*det) - ct*cp*sa*sb/(r*np.sqrt(gthth))
            res[6] = st*sp*ca/np.sqrt(grr) + cp*sa*cb*np.sqrt(gtt*det) - ct*sp*sa*sb/(r*np.sqrt(gthth))
            res[7] = ct*ca/np.sqrt(grr) + st*sa*sb/(r*np.sqrt(gthth))
            return res+[0.0, 0.0, -1.0]
        elif len(x)==8:
            # full state
            return list(x)+[0.0, 0.0, -1.0]
        else:
            # unknown
            return x

    def getNextStep(self, l, x, dl):
        """Get the next integration time step for the current integration.

        :param l: current integration time lambda
        :param x: current integration state
        :param dl: the last integration time step.
        """
        y = self(l, x)
        if self.Rsky == 0.0:
            # integrating backward => outward leg has negative radial change
            xdotv = (x[1]*y[1]+x[2]*y[2]+x[3]*y[3])/(sqrt(x[1]*x[1]+x[2]*x[2]+x[3]*x[3])*sqrt(y[1]*y[1]+y[2]*y[2]+y[3]*y[3]))
            done = abs(xdotv+1.0)<1e-3   # on an outward leg and essentially moving radially
            if done:
                return 0.0
            else:
                return dl
        else:
            # cutoff at given fixed radius
            r = sqrt(x[1]*x[1]+x[2]*x[2]+x[3]*x[3])
            # are we there?
            if abs(r-self.Rsky)<1e-2:
                return 0.0
            # are we close?
            xdotv = x[1]*y[1]+x[2]*y[2]+x[3]*y[3]
            if not self.finishing:
                self.finishing = xdotv<0.0 and r>0.95*self.Rsky    # on outward leg and close enough to Rsky
            # if we're in the finishing stage, do a Newton to get to Rsky
            if self.finishing:
                drdl = xdotv/r
                return 0.95*(self.Rsky-r)/drdl       # damping factor to avoid overshooting
            else:
                return dl

    def getFinalDirection(self, l, x, escaped):
        """Get the final direction where the light ray is coming from.

        :param l: Independent integration variable lambda.
        :param x: final integration state.
        :param escaped: flag to indicate if this is the final direction of a ray that escaped (True) or one that fell into a black hole (False)
        """
        if self.Rsky == 0.0 and escaped:
            # use velocity direction
            df = self(l, x)
            X = df[1]
            Y = df[2]
            Z = df[3]
        else:
            # use current point
            X = x[1]
            Y = x[2]
            Z = x[3]
        theta = acos(Z/sqrt(X*X+Y*Y+Z*Z))
        phi = atan2(Y, X)%(2.0*np.pi)
        return (theta,phi)

    def __call__(self, l, x):
        """Right hand side of the ODE for propagation.

        :param l: Independent integration variable lambda.
        :param x: Point where to evaluate the RHS.
        """
        y = np.zeros(11)
        if self.error or self.g.setPoint(x):
            self.error = True
            return y
        y[0] = self.g.tt*x[4] + self.g.tx*x[5] + self.g.ty*x[6]
        y[1] = self.g.tx*x[4] + self.g.xx*x[5] + self.g.xy*x[6] + self.g.xz*x[7]
        y[2] = self.g.ty*x[4] + self.g.xy*x[5] + self.g.yy*x[6] + self.g.yz*x[7]
        y[3] =                  self.g.xz*x[5] + self.g.yz*x[6] + self.g.zz*x[7]
#        y[4] = 0.0
        y[5] = -0.5*(self.g.dx_tt*x[4]*x[4] + 2.0*self.g.dx_tx*x[4]*x[5] + 2.0*self.g.dx_ty*x[4]*x[6] + self.g.dx_xx*x[5]*x[5] + self.g.dx_yy*x[6]*x[6] + self.g.dx_zz*x[7]*x[7] + 2.0*self.g.dx_xy*x[5]*x[6] + 2.0*self.g.dx_xz*x[5]*x[7] + 2.0*self.g.dx_yz*x[6]*x[7])
        y[6] = -0.5*(self.g.dy_tt*x[4]*x[4] + 2.0*self.g.dy_tx*x[4]*x[5] + 2.0*self.g.dy_ty*x[4]*x[6] + self.g.dy_xx*x[5]*x[5] + self.g.dy_yy*x[6]*x[6] + self.g.dy_zz*x[7]*x[7] + 2.0*self.g.dy_xy*x[5]*x[6] + 2.0*self.g.dy_xz*x[5]*x[7] + 2.0*self.g.dy_yz*x[6]*x[7])
        y[7] = -0.5*(self.g.dz_tt*x[4]*x[4] + 2.0*self.g.dz_tx*x[4]*x[5] + 2.0*self.g.dz_ty*x[4]*x[6] + self.g.dz_xx*x[5]*x[5] + self.g.dz_yy*x[6]*x[6] + self.g.dz_zz*x[7]*x[7] + 2.0*self.g.dz_xy*x[5]*x[6] + 2.0*self.g.dz_xz*x[5]*x[7] + 2.0*self.g.dz_yz*x[6]*x[7])
        y[8] = (y[2]*x[1]-y[1]*x[2])/max(0.1, x[1]*x[1]+x[2]*x[2])      # dphi/dlambda, with protection to prevent divisions by small numbers (when closer than 0.01 to axis)
        y[9] = abs(y[8])     # |dphi/dlambda|
#        y[10] = 0.0        # null condition violation, not computed on CPU
        return y
