import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image
from gr_pyhole.display import Display

# 1) Set up one of the metrics provided with PyHole
g = metric.Kerr(2.0, 1.0)

# 2) set up an observer
o = observer.Equirectangular(r=15.0, theta=np.pi/2)

# 3) set up a propagator
p = propagator.SphericalCPU(o, g, Rsky=30.0)
p.TOLERANCE = 1e-6        # propagator error tolerance

# generate and save an image
i = Image(p, (128,128))   # (128,128) is size in pixels.
i.saveImage('ex1.png')
i.save('ex1.npz')
print(str(i))             # prints info about image

d = Display(i, p)         # create new Display
d.show()
