pyhole package
==============

License
-------
   Copyright 2015 - 2017 Alexander Wittig, Jai Grover

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


Citing PyHole
-------------
We spend a lot of our time and effort on the development of PyHole. We hope you enjoy using it and, we are delighted if it is of use for your research.

If you used PyHole in your research which then leads to a scientific publication, in accordance with common scientific practice, we ask you to cite PyHole as one of your references. 
Please cite the following publication:

	Grover, J. and Wittig, A., *PyHole: A general relativity ray tracing toolbox*, Computer Physics Communications, in preparation (2017)

For your convenience, here is the BibTeX entry for this publication::

  @Article{2016PyHole,
      author       = {Grover, J. and Wittig, A.},
      title        = {{PyHole}: A general relativity ray tracing toolbox},
      journal      = {Computer Physics Communications},
      volume       = {in preparation},
      year         = {2017},
  }


Subpackages
-----------

.. toctree::

    gr_pyhole.metric
    gr_pyhole.observer
    gr_pyhole.propagator

Submodules
----------

gr_pyhole.display module
---------------------

.. automodule:: gr_pyhole.display
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.image module
-------------------

.. automodule:: gr_pyhole.image
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.scene module
-------------------

.. automodule:: gr_pyhole.scene
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gr_pyhole
    :members:
    :undoc-members:
    :show-inheritance:
